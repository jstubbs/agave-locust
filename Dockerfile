# Image: agave-locust

from alpine:3.2
RUN apk add --update g++
# RUN apk add --update make
RUN apk add --update python-dev
RUN apk add --update bash && rm -rf /var/cache/apk/*
RUN apk add --update git
RUN apk add --update py-pip

ADD requirements.txt /requirements.txt
RUN pip install -r /requirements.txt

ADD code /code

EXPOSE 8089

CMD ["locust", "-f", "/code/locustfile.py"]
