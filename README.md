# Agave Locust Load Testing #

## Overview ##
This project can be used to load test an instance of the Agave platform. It exercises the full stack including all
auth services (e.g. the OAuth token service). It leverages the python locust project (http://docs.locust.io/en/latest/)
which we think is pretty nifty in its own right.

## Set up ##
Pull down the docker image (jstubbs/agave-locust) and run on any docker host. You need to mount in a configuration file
to instruct agave-locust what server to test, etc. Look at the test-credentials-example.json inside the code directory
to see what is needed.

## Execute the tests ##
Issue something similar to the following command on the execution host:

```
#!bash

$ docker run --rm -it -p 8089:8089 -v $(pwd)/test-credentials-staging.json:/code/test-credentials.json jstubbs/agave-locust
```

and navigate to

http://localhost:8089/ to see the locust UI. Start up any number of users.



