import json
import os
HERE = os.path.dirname(os.path.abspath(__file__))

class TestData(object):

    def __init__(self, credentials):
        self.local_data = credentials

    def file_to_json(self, filename):
        return json.load(open(os.path.join(HERE, filename)))

    def get_test_storage_system(self):
        """
        Example storage system read from an external file.
        """
        storage = self.file_to_json('systems/storage.json')
        if 'use_rodeo' in self.local_data and self.local_data['use_rodeo']:
            storage = self.file_to_json('systems/storage-rodeo.json')
        if 'storage_publicKey' in self.local_data:
            storage['storage']['auth']['type'] = 'SSHKEYS'
            storage['storage']['auth']['publicKey'] = self.local_data['storage_publicKey']
            storage['storage']['auth']['privateKey'] = self.local_data['storage_privateKey']
        else:
            storage['storage']['auth']['password'] = self.local_data['storage_password']
        return storage

    def get_test_storage_system_2(self):
        storage = self.get_test_storage_system()
        storage['id'] += '_2'
        return storage

    def get_test_compute_system(self):
        """
        Example compute system defined inline.
        """
        compute = self.file_to_json('systems/execute.json')
        if 'use_rodeo' in self.local_data and self.local_data['use_rodeo']:
            compute = self.file_to_json('systems/execute-rodeo.json')
        if 'execution_publicKey' in self.local_data:
            compute['storage']['auth']['type'] = 'SSHKEYS'
            compute['storage']['auth']['publicKey'] = self.local_data['execution_publicKey']
            compute['storage']['auth']['privateKey'] = self.local_data['execution_privateKey']
            compute['login']['auth']['type'] = 'SSHKEYS'
            compute['login']['auth']['publicKey'] = self.local_data['execution_publicKey']
            compute['login']['auth']['privateKey'] = self.local_data['execution_privateKey']
        else:
            compute['storage']['auth']['password'] = self.local_data['execution_password']
            compute['login']['auth']['password'] = self.local_data['execution_password']
        return compute

    def get_bigdata_storage_system(self):
        """
        Example storage system read from an external file.
        """
        return self.get_test_storage_system()
        # storage = self.file_to_json('test-storage-bigdata.json')
        # storage['id'] = self.local_data['storage']
        # storage['storage']['auth']['password'] = self.local_data['storage_password']
        # return storage

    def get_bigdata_compute_system(self):
        """
        Example compute system defined inline.
        """
        return self.get_test_compute_system()
        # compute = self.file_to_json('test-compute-bigdata.json')
        # compute['id'] = self.local_data['execution']
        # compute['login']['auth']['password'] = self.local_data['execution_password']
        # compute['storage']['auth']['password'] = self.local_data['storage_password']
        # return compute

    def get_test_app_from_file(self):
        app = self.file_to_json('test-app-staging.json')
        if 'use_rodeo' in self.local_data and self.local_data['use_rodeo']:
            app = self.file_to_json('test-app-staging-rodeo.json')
        # app['name'] += self.local_data['app_name'] + '_'
        return app

    def get_long_running_app_from_file(self):
        return self.get_test_app_from_file()
        # app = self.file_to_json('test-long-running-app.json')
        # if 'app_name_prefix' in self.local_data:
        #     app['name'] += self.local_data['app_name_prefix'] + '_'
        # return app


    def get_test_job_from_file(self):
        """
        Example job request read in from an external file.
        """
        test_app = self.get_test_app_from_file()
        job = self.file_to_json('test-job-staging.json')
        # job['appId'] = test_app['name'] + '-' + test_app['version']
        return job

    def get_test_longrun_job_from_file(self, sleep_length=10):
        return self.get_test_job_from_file()
        # job = self.file_to_json('test-long-running-job.json')
        # job['parameters']['sleep'] = sleep_length
        # return job
