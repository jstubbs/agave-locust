from functools import partial
import json
import os
import time

from locust import HttpLocust, TaskSet, task, events
import requests

import agavepy.agave as a

import testdata

HERE = os.path.dirname(os.path.abspath(__file__))

def credentials():
    # return json.load(open('/home/jstubbs/bitbucket-repos/agave-locust/test-credentials-sandbox.json'))
    return json.load(open(
        os.path.join(HERE, 'test-credentials.json')))

credentials = credentials()

# we cannot test the clients service when using a JWT since the service URL points to agave-core and clients is
# deployed on agave-auth
test_clients = True

def load_resources():
    if credentials.get('jwt') and credentials.get('jwt_header_name'):
        aga = a.Agave(jwt=credentials['jwt'],
                      jwt_header_name=credentials['jwt_header_name'],
                      api_server=credentials['apiserver'],
                      verify=credentials.get('verify_certs', True))
        test_clients = False
    else:
        aga = a.Agave(username=credentials['username'],
                      password=credentials['password'],
                      api_server=credentials['apiserver'],
                      api_key=credentials['apikey'],
                      api_secret=credentials['apisecret'],
                      verify=credentials.get('verify_certs', True))
        aga.token.create()
    return aga


class BasicAgaveTasks(TaskSet):

    def test_app(self):
        return testdata.TestData(credentials).get_test_app_from_file()

    def long_running_app(self):
        return testdata.TestData(credentials).get_long_running_app_from_file()

    def test_job(self):
        return testdata.TestData(credentials).get_test_job_from_file()

    def test_longrun_job(self, sleep_length=10):
        return testdata.TestData(credentials).get_test_longrun_job_from_file(sleep_length)

    def test_compute_system(self):
        return testdata.TestData(credentials).get_test_compute_system()

    def test_storage_system(self):
        return testdata.TestData(credentials).get_test_storage_system()

    def test_storage_system_2(self):
        return testdata.TestData(credentials).get_test_storage_system_2()

    def on_start(self):
        self.name = 'agave'
        self.agave = load_resources()
        self.register_dependencies()

    def register_dependencies(self):
        """Register the dependent systems and apps before starting the test suite."""
        self.add_storage_system()
        self.add_execution_system()
        self.add_app()
        self.add_long_running_app()

    def add_long_running_app(self):
        """Register a long running application"""
        test_app = self.long_running_app()
        p = partial(self.agave.apps.add, body=test_app)
        self.call_partial('apps-add-longrun', p)

    def call_partial(self, name, p):
        """Call a partial with error handling"""
        start_time = time.time()
        try:
            p()
        except Exception as e:
            total_time = int((time.time() - start_time) * 1000)
            events.request_failure.fire(request_type="http", name=name, response_time=total_time, exception=e)
        else:
            total_time = int((time.time() - start_time) * 1000)
            events.request_success.fire(request_type="http", name=name, response_time=total_time, response_length=0)

    # @task(10)
    # def move_large_file(self):
    #    storage_system = self.test_storage_system()
    #    url = 'agave://{}//home/{}/bigdata/bigfile.txt'.format((storage_system['id'],
    #                                                            credentials['username']))
    #    storage_system_2 = self.test_storage_system_2()
    #    p = partial(self.agave.files.importData,
    #                systemId=storage_system_2.get('id'),
    #                filePath="/home/{}/bigdata".format(credentials['username']),
    #                urlToIngest=url)
    #
    @task(10)
    def large_file_listing(self):
        storage_system = self.test_storage_system()
        p = partial(self.agave.files.list,
                    systemId=storage_system.get('id'),
                    filePath='/home/jdoe/testdata/large')
        self.call_partial('files-list-10k', p)

    @task(10)
    def list_files(self):
        storage_system = self.test_storage_system()
        p = partial(self.agave.files.list,
                    systemId=storage_system.get('id'),
                    filePath="/home/jdoe/testdata")
        self.call_partial('files-list', p)

    @task(10)
    def list_apps(self):
        p = partial(self.agave.apps.list)
        self.call_partial('apps-list', p)

    @task(10)
    def list_systems(self):
        p = partial(self.agave.systems.list)
        self.call_partial('systems-list', p)

    @task(10)
    def list_jobs(self):
        p = partial(self.agave.jobs.list)
        self.call_partial('jobs-list', p)

    @task(10)
    def list_clients(self):
        if not test_clients:
            return
        p = partial(self.agave.clients.list)
        self.call_partial('clients-list', p)

    @task(10)
    def add_app(self):
        test_app = self.test_app()
        p = partial(self.agave.apps.add, body=test_app)
        self.call_partial('apps-add', p)

    @task(10)
    def submit_job(self):
        test_job = self.test_job()
        p = partial(self.agave.jobs.submit, body=test_job)
        print "Job json:", str(test_job)
        self.call_partial('jobs-submit', p)

    @task(10)
    def submit_longrun_job(self):
        test_job = self.test_longrun_job()
        p = partial(self.agave.jobs.submit, body=test_job)
        self.call_partial('jobs-submit-longrun', p)

    @task(10)
    def add_storage_system(self):
        test_storage_system = self.test_storage_system()
        p = partial(self.agave.systems.add, body=test_storage_system)
        self.call_partial('systems-add', p)

    @task(10)
    def add_execution_system(self):
        test_compute_system = self.test_compute_system()
        p = partial(self.agave.systems.add, body=test_compute_system)
        self.call_partial('systems-add', p)

    # @task(100)
    # def download_file(self):

    # @task(10000)
    # def create_client(self):


class BasicAgaveLocust(HttpLocust):
    task_set = BasicAgaveTasks
    host = credentials['apiserver']
    min_wait = 800
    max_wait = 800

